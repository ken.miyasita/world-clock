import { RawTimeZone,rawTimeZones } from '@vvo/tzdb';
import * as d3 from 'd3';
import * as GeoJSON from 'geojson';
import React, { ReactElement } from 'react';
import * as topojson from 'topojson-client';
// eslint-disable-next-line import/no-unresolved
import { Topology } from 'topojson-specification';

import timezoneTopoJson from './assets/timezones.json';

type PolygonFeature = GeoJSON.Feature<
  GeoJSON.Polygon,
  GeoJSON.GeoJsonProperties
>;

/**
 * Read world map polygon data.
 * @returns array of polygon data
 */
const createTimeZonePolygonFeatures = (): PolygonFeature[] => {
  // Read world map for timezones.
  // See  https://raw.githubusercontent.com/smallwins/spacetime/gh-pages/lib/timezones.json

  // Somehow TS type definition does not match with the actual data, and I need to resort to
  // forceful casting.
  const tzData: Topology = (timezoneTopoJson as unknown) as Topology;
  const tzDataFeature = topojson.feature(tzData, tzData.objects.timezones);
  const features = (tzDataFeature as { features: PolygonFeature[] }).features;
  return features;
};

/**
 * Find a time zone data in @vvo/tzdb.
 * @param timeZoneName - Time zone name.
 * @return Time zone data in @vvo/tzdb.
 */
const findTimeZone = (timeZoneName: string): RawTimeZone => {
  return rawTimeZones.find((timeZone) => {
    return (
      timeZoneName === timeZone.name || timeZone.group.includes(timeZoneName)
    );
  });
};

interface WorldMapProps {
  /** Time zone name selected e.g. "Asia/Tokyo" */
  timeZoneName: string;
  /** Called when a timezone is selected. */
  onChange: (timeZoneName: string) => void;
}

const WorldMap = (props: WorldMapProps): ReactElement => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleClick = (e: any) => {
    props.onChange(findTimeZone(e.target.id).name);
  };

  const pathGenerator = d3.geoPath();
  const timeZonePolygonFeatures = React.useMemo(
    createTimeZonePolygonFeatures,
    []
  );
  const selectedTimeZone = findTimeZone(props.timeZoneName);
  const tzPaths = timeZonePolygonFeatures.map((d: PolygonFeature) => {
    // Time zone corresponding to the polygon.
    const timeZone = findTimeZone(`${d.id}`) || {
      countryName: `${d.id}`,
      mainCities: [`${d.id}`],
      rawOffsetInMinutes: 100000, // invalid value to avoid matching with any existing time zones
    };
    const id = `${d.id}`;
    let opacity;
    let stroke;
    if (selectedTimeZone === timeZone) {
      opacity = 1.0;
      stroke = 'darkgrey';
    } else if (
      selectedTimeZone.rawOffsetInMinutes === timeZone.rawOffsetInMinutes
    ) {
      opacity = 0.7;
      stroke = 'grey';
    } else {
      opacity = 0.4;
      stroke = 'lightgrey';
    }

    const title = `${timeZone.countryName} / ${timeZone.mainCities[0]}`;
    return (
      <path
        id={id}
        key={id}
        d={pathGenerator(d)}
        opacity={opacity}
        fill={'lightgrey'}
        strokeWidth={2}
        stroke={stroke}
        onClick={handleClick}
      >
        <title>{title}</title>
      </path>
    );
  });
  return (
    <svg viewBox={'0 0 960 500'} width={700} >
      <g style={{ cursor: 'pointer' }} transform={'translate(0, -175)'}>
        {tzPaths}
      </g>
    </svg>
  );
};

export default WorldMap;
