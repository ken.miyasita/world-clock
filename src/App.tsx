import * as React from 'react';
import * as ReactDOM from 'react-dom';

import TimeTableScreen from './TimeTableScreen';


function App() {
  return (
    <div>
      <TimeTableScreen />
    </div>
  );
}

ReactDOM.render(<App />, document.querySelector('#root'));
