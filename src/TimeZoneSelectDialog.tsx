import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import React, { ReactElement } from 'react';

import TimeZoneSelect from './TimeZoneSelect';
import WorldMap from './WorldMap';

const useStyles = makeStyles(() => ({
  worldMap: {
    marginTop: 10
  }
}));

enum State {
  Closed = 'CLOSED',
  Open = 'OPEN',
}

export interface TimeZoneSelectDialogProps {
  open: boolean;
  timeZoneName: string;
  onClose: (newTimeZoneName: string) => void;
}

function TimeZoneSelectDialog(props: TimeZoneSelectDialogProps): ReactElement {
  const { open, onClose } = props;
  const [state, setState] = React.useState(State.Closed);
  const [timeZoneName, setTimeZoneName] = React.useState(props.timeZoneName);
  const classes = useStyles();

  // Initialize the time zone on opening the dialog.
  React.useEffect(() => {
    if (state === State.Closed && open) {
      setState(State.Open);
      setTimeZoneName(props.timeZoneName);
    }
  }, [open, state]);

  const handleOK = () => {
    setState(State.Closed);
    onClose(timeZoneName);
  };

  const handleCancel = () => {
    setState(State.Closed);
    onClose(props.timeZoneName); // revert
  };

  const handleTimeZoneChange = (newTimeZoneName: string) => {
    setTimeZoneName(newTimeZoneName);
  };

  return (
    <Dialog
      onClose={handleCancel}
      aria-labelledby="time-zone-select-dialog-title"
      open={open}
      maxWidth={'lg'}
    >
      <DialogTitle id="time-zone-select-dialog-title">Time Zone</DialogTitle>
      <DialogContent>
        <Container>
          <Box>
            <DialogContentText>
              Select a time zone in the list or click an area on the map.
            </DialogContentText>
          </Box>
          <Box>
            <TimeZoneSelect
              timeZoneName={timeZoneName}
              onChange={handleTimeZoneChange}
            />
          </Box>
          <Box className={classes.worldMap}>
            <WorldMap
              timeZoneName={timeZoneName}
              onChange={handleTimeZoneChange}
            />
          </Box>
        </Container>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancel} variant="contained">
          Cancel
        </Button>
        <Button onClick={handleOK} variant="contained" color="primary">
          OK
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default TimeZoneSelectDialog;
