import { RawTimeZone, rawTimeZones } from '@vvo/tzdb';

/**
 * Find a time zone information by name.
 * @param timeZoneName  Time zone name e.g. "America/Los_Angeles"
 * @returns  Time zone information
 */
export function findTimeZoneByName(timeZoneName: string): RawTimeZone {
    return rawTimeZones.find((tz) => tz.name === timeZoneName);
}

function convertNumberToStringWithZeroPadding(num: number): string {
    return ('0' + num).slice(-2);
}

/**
 * Convert time offset to user friendly string.
 * e.g.  -60 ==> "-01:00"
 * @param offsetInMinutes  offset from UTC in minutes.
 * @returns  user friendly string e.g. "-01:00"
 */
export function convertOffsetInMinutesToString(offsetInMinutes: number): string {
    const absValue = Math.abs(offsetInMinutes);
    const hour = Math.floor(absValue / 60);
    const minute = absValue % 60;
    const plusMinus = offsetInMinutes >= 0 ? '+' : '-';

    return (
        plusMinus +
        convertNumberToStringWithZeroPadding(hour) +
        ':' +
        convertNumberToStringWithZeroPadding(minute)
    );
}

/**
 * Create a string which has the specified substring left aligned with gap filler.
 * @param subString  substring aligned to left.
 * @param totalLength  length of the created string.
 * @returns  aligned string
 */
export function createLeftAlignedString(subString: string, totalLength: number): string {
    const subStringLength = subString.length;
    if (subStringLength >= totalLength) {
        // Truncate.
        return subString.slice(0, totalLength);
    } else {
        // Add gap filler.
        return subString + ' '.repeat(totalLength - subStringLength);
    }
}
